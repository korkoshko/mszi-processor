<?php

require_once __DIR__ . '/../vendor/autoload.php';

use korkoshko\{
    Processor,
    Request,
};

$intensities = [
    25,
    50,
    75,
    100,
];

foreach ($intensities as $intensity) {
    $waiting = startTest($intensity);
    $numberOfCompletedRequests = count($waiting);

    echo 'Intensity: ' . $intensity . ' | ' .
        'Requests: ' . $numberOfCompletedRequests . ' | ' .
        'Average waiting time: ' . array_sum($waiting) / $numberOfCompletedRequests
        . PHP_EOL;
}

function startTest($intensity, $executing = 60)
{
    $processor = (new Processor)
        ->addCore()
        ->addCore();

    $queue = $processor->getQueue();

    $waiting = [];

    $startTime = time();

    do {
        if (random_int(0, 100) <= $intensity) {
            $handlerTime = random_int(0, 15);
            $queue->push(
                new Request($handlerTime)
            );
        }

        foreach ($processor->getCores() as $core) {
            if (!$queue->count()) {
                break;
            }

            $request = $core->getRequest();

            if ($request->isCompleted()) {
                $core->setRequest($queue->pop());

                $waiting[] = $request->getWaitingTime();
            }

            $request->processing();
        }
    } while ((time() - $startTime) < $executing);

    return $waiting;
}