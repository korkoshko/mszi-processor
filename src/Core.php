<?php

namespace korkoshko;

class Core
{
    /**
     * @var Request|null
     */
    protected ?Request $request;

    /**
     * Core constructor.
     */
    public function __construct()
    {
        $this->request = new Request(0);
    }

    /**
     * @param Request|null $request
     *
     * @return Request
     */
    public function setRequest(?Request $request): self
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return $this
     */
    public function getRequest(): Request
    {
        return $this->request;
    }
}