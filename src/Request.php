<?php

namespace korkoshko;

class Request
{
    /**
     * @var int
     */
    protected int $startTime;

    /**
     * @var int
     */
    protected int $handlerTime;

    /**
     * Request constructor.
     *
     * @param int $handlerTime
     */
    public function __construct(int $handlerTime)
    {
        $this->handlerTime = $handlerTime;
        $this->startTime = microtime(true);
    }

    /**
     * @return int
     */
    public function processing(): int
    {
        if ($this->handlerTime) {
            $this->handlerTime--;
        }

        return $this->handlerTime;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->handlerTime < 1;
    }

    /**
     * @return float|int|string
     */
    public function getWaitingTime(): float
    {
        return microtime(true) - $this->startTime;
    }
}