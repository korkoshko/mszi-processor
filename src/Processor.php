<?php

namespace korkoshko;

class Processor
{
    /**
     * @var array
     */
    protected array $cores;

    /**
     * @var Queue
     */
    protected Queue $queue;

    /**
     * Processor constructor.
     */
    public function __construct()
    {
        $this->cores = [];
        $this->queue = new Queue;
    }

    /**
     * @return $this
     */
    public function addCore(): self
    {
        array_push($this->cores, new Core);
        return $this;
    }

    /**
     * @return array
     */
    public function getCores(): array
    {
        return $this->cores;
    }

    /**
     * @return int
     */
    public function getCountOfCores(): int
    {
        return count($this->cores);
    }

    /**
     * @return Queue
     */
    public function getQueue(): Queue
    {
        return $this->queue;
    }
}