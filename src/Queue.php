<?php

namespace korkoshko;

class Queue
{
    /**
     * @var array
     */
    protected array $requests = [];

    /**
     * @var int
     */
    protected int $counter = 0;

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function push(Request $request): self
    {
        $this->increment();
        array_push($this->requests, $request);
        return $this;
    }

    /**
     * @return mixed
     */
    public function pop(): Request
    {
        $this->decrement();
        return array_shift($this->requests);
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return $this->counter;
    }

    /**
     * @return int
     */
    private function increment(): int
    {
        return ++$this->counter;
    }

    /**
     * @return int
     */
    private function decrement(): int
    {
        return --$this->counter;
    }
}